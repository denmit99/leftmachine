import java.util.ArrayList;

public class Grammar {
    private ArrayList<Rule> rules = new ArrayList<Rule>();

    public void addRule(Rule r) {
        rules.add(r);
    }

    public ArrayList<String> getTransitions(String leftPart) {

        /* Возвращает массив правых частей правил из грамматики по левой leftPart */

        ArrayList<String> transitions = new ArrayList<String>();
        for (Rule r : rules) {
            if (r.getLeftPart().equals(leftPart)) {
                transitions.add(r.getRightPart());
            }
        }
        return transitions;
    }
}