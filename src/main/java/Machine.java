import java.util.*;

public class Machine {

    private char startSymbol; //стартовый терминал
    private Grammar grammar; //грамматика, набор правил
    private final char[] terminals; //массив терминальных символов
    private final char[] nonTerminals; //массив нетерминальных символов

    public Machine(char startSymbol, Grammar grammar, char[] terminals, char[] nonTerminals) {
        this.startSymbol = startSymbol;
        this.grammar = grammar;
        this.terminals = terminals;
        this.nonTerminals = nonTerminals;
    }

    public boolean checkChain(String chain) {

       /*
        * Проверка на сооответствие цепочки символов автомату.
        * Все возможные подстановки на место терминальных символов добавляются в очередь transitions
        * (все правила из грамматики, которые можно применить, применяются).
        * По возможности первый символ в цепочке и в магазинной памяти затирается.
        * Как только найдена верная последовательность правил, распознающая цепочку, метод возвращает true.
        * Если очередь стала пустой, следовательно больше ни одно правило применить нельзя, метод вовзращает false.
       */

        LinkedList<State> transitions = new LinkedList<State>();
        transitions.add(new State(chain.toCharArray(), String.valueOf(startSymbol).toCharArray()));
        while (true) {
            if (transitions.isEmpty()) {
                return false;
            }
            State nextState = transitions.peek();
            nextState.reduceTop();
            if (nextState.isFinished()) {
                return true;
            }
            if (deadlock(nextState)) {
                transitions.poll();
                continue;
            }
            if (isTerminal(nextState.getStack().peek())) {
                addStates(nextState.getStack().peek(), nextState, transitions);
                transitions.poll();
            }
        }
    }

    public boolean isTerminal(char s) {

        /*
        * Возвращает true, если символ s терминальный
        * */

        for (int i = 0; i < terminals.length; i++) {
            if (s == terminals[i]) {
                return true;
            }
        }
        return false;
    }

    public boolean isNonTerminal(char s) {

        /*
         * Возвращает true, если символ s нетерминальный
         * */

        for (int i = 0; i < nonTerminals.length; i++) {
            if (s == nonTerminals[i]) {
                return true;
            }
        }
        return false;
    }

    public boolean deadlock(State state) {

        /*
        * Возвращает true если текущее состояние тупиковое (невозможно применить ни одно правило)
        * */

        if ((state.getChain().size() > 0) && (state.getStack().empty())) { //если стек пуст, но цепочка еще не распознана
            return true;
        }

        char stackPeek = state.getStack().peek().charValue();
        char chainPeek = state.getChain().peek().charValue();

        if (isNonTerminal(stackPeek) && (stackPeek != chainPeek)) {//если на верхушках двух стеков разные нетерминальные символы
            return true;
        }
        return false;
    }

    public void addStates(char terminal, State state, LinkedList<State> transitions) {

        /*
        * Применение всех возможных правил и добавление новых состояний в очередь
        * */

        for (String s : grammar.getTransitions(String.valueOf(terminal))) {

            Stack<Character> chain = new Stack<Character>();
            Stack<Character> stack = new Stack<Character>();
            Stack<Character> originalStack = state.getStack();
            Stack<Character> originalChain = state.getChain();
            for (int i = 0; i < originalChain.size(); i++) {
                chain.add(originalChain.get(i));
            }
            for (int i = 0; i < originalStack.size() - 1; i++) {
                stack.add(originalStack.get(i));
            }
            char[] symbols = s.toCharArray();
            for (int i = 0; i < symbols.length; i++) {
                stack.push(symbols[symbols.length - 1 - i]);
            }
            if (!(stack.size() > chain.size())) { //не использовать правило если получается слишком длинная строка
                transitions.add(new State(chain, stack));
            }
        }
    }

    public void printStack(LinkedList<State> transitions) {

        /*
        * Печатает стек текущих непроверенных правил
        * */

        for (State s : transitions) {
            String chainLine = "", stackLine = "";
            for (int i = 0; i < s.getStack().size(); i++) {
                stackLine += s.getStack().get(s.getStack().size() - 1 - i);
            }
            for (int i = 0; i < s.getChain().size(); i++) {
                chainLine += s.getChain().get(s.getChain().size() - 1 - i);
            }
            System.out.println("(" + chainLine + "," + stackLine + ")");
        }
        System.out.println("________");
    }
}