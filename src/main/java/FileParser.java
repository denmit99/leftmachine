import java.io.*;

public class FileParser {

    public Grammar parseFile(File file) {

        /* Создает грамматику по текстовому файлу */

        Grammar grammar = new Grammar();
        try {
            FileReader fr = new FileReader(file);
            BufferedReader reader = new BufferedReader(fr);

            try {
                String line = reader.readLine();
                while (line != null) {
                    Rule rule = new Rule(line);
                    grammar.addRule(rule);
                    line = reader.readLine();
                }
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
            return grammar;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }
}