import java.util.Stack;

public class State {

    private Stack<Character> chain = new Stack<Character>(); //цепочка символов
    private Stack<Character> stack = new Stack<Character>(); //магазинная память

    public State(char[] chain, char[] stack) {
        for (int i = 0; i < chain.length; i++) {
            this.chain.push(chain[chain.length - 1 - i]);
        }
        for (int i = 0; i < stack.length; i++) {
            this.stack.push(stack[stack.length - 1 - i]);
        }
    }

    public State(Stack<Character> chain, Stack<Character> stack) {
        this.chain = chain;
        this.stack = stack;
    }

    public void reduceTop() {

        /* Съесть верхушки обоих стеков, если это возможно */

        while (!(chain.empty()||(stack.empty()))&&(chain.peek().charValue() == stack.peek().charValue())) {
            stack.pop();
            chain.pop();
        }
    }

    public boolean isFinished() {

        /* Возвращает true, если цепочка символов полностью съедена т.е. распознана*/

        if (chain.empty()) {
            return true;
        }
        return false;
    }

    Stack<Character> getChain() {
        return chain;
    }

    Stack<Character> getStack() {
        return stack;
    }
}